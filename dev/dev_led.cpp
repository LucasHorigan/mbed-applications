/*
 * file: dev_led.cpp
 * purpose: Driver for LED objects using the mbed framework.
 */

#include "dev_led.h"

namespace DEV
{

LED::LED(IO::GPIO_INTF & gpio_pin, ACTIVE_STATE configuration) : my_gpio_pin(gpio_pin), my_active_state(configuration)
{
    this->my_gpio_pin.setPinDirection(IO::GPIO_INTF::DIRECTION::OUTPUT);
    this->setCurrentState(IO::GPIO_INTF::STATE::LOW);
}


LED::~LED()
{
    // Empty destructor
}


void LED::toggle()
{
    IO::GPIO_INTF::STATE current_state = this->my_gpio_pin.readPin();

    if (IO::GPIO_INTF::STATE::LOW == current_state)
    {
        this->my_gpio_pin.writePin(IO::GPIO_INTF::STATE::HIGH);
    }
    else
    {
        this->my_gpio_pin.writePin(IO::GPIO_INTF::STATE::LOW);
    }
}


IO::GPIO_INTF::STATE LED::getCurrentState()
{
    return this->my_gpio_pin.readPin();
}


void LED::setCurrentState(IO::GPIO_INTF::STATE state)
{
    // if the LED is active high, it's state follows from pin setting
    if (this->my_active_state == ACTIVE_STATE::HIGH)
    {
        this->my_gpio_pin.writePin(state);
    }
    // otherwise, it is reversed
    else
    {
        if (state == IO::GPIO_INTF::STATE::HIGH)
        {
            this->my_gpio_pin.writePin(IO::GPIO_INTF::STATE::LOW);
        }
        else
        {
            this->my_gpio_pin.writePin(IO::GPIO_INTF::STATE::HIGH);
        }
    }
}

} // namespace DEV
