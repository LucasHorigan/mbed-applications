/*
 * file: io_i2c_intf.h
 * purpose: Define the interface for I2C.
 */

#ifndef IO_I2C_INTF_H
#define IO_I2C_INTF_H

#include <cstdint>

namespace IO
{

class i2cIntf
{
public:

    // Empty constructor
    i2cIntf() {}
    // Empty destructor
    virtual ~i2cIntf() {}
    
    /** 
     *  write - Writes a string of data to an I2C slave.
     *
     *  Parameters:
     *      addr    - The unshifted 7-bit address of the I2C slave that will
     *                  be written to.
     *      data    - The pointer to the array of bytes that will be written
     *                  to the slave.
     *      length  - The number of bytes that will be written to the slave. 
     */
    virtual void write(uint8_t addr, char *data, int length) = 0;

    /**
     *  read - Reads a string of data from an I2C slave.
     *
     *  Parameters:
     *      addr    - The unshifted 7-bit address of the I2C slave that will
     *                  be read from.
     *      data    - The pointer to the buffer that will be used to store the
     *                  data received from the slave.
     *      length  - The number of bytes that will be read from the slave. 
     */
    virtual void read(uint8_t addr, char *data, int length) = 0;

}; // class I2C_INTF

} // namespace IO
#endif // IO_I2C_INTF_H
