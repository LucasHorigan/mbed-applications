/*
 * file: io_gpio_mbed_adapter.cpp
 * purpose: Implementation of the GPIO driver using Mbed framework.
 */

#include "io_gpio_mbed_adapter.h"


namespace IO
{

GPIO_MBED_ADAPTER::GPIO_MBED_ADAPTER(PIN pin) : my_pin((PinName)pin)
{
    // Empty constructor
}


GPIO_MBED_ADAPTER::~GPIO_MBED_ADAPTER()
{
    // Empty destructor
}


void GPIO_MBED_ADAPTER::setPinDirection(DIRECTION direction)
{
    switch (direction)
    {
        case DIRECTION::INPUT:
        {
            this->my_pin.input();
            break;
        }
        case DIRECTION::OUTPUT:
        {
            this->my_pin.output();
            break;
        }
        default:
        {
            break;
        }
    }
}


void GPIO_MBED_ADAPTER::writePin(STATE state)
{
    switch (state)
    {
        case STATE::LOW:
        {
            this->my_pin = 0;
            break;
        }
        case STATE::HIGH:
        {
            this->my_pin = 1;
            break;
        }
        default:
        {
            break;
        }
    }
}


GPIO_MBED_ADAPTER::STATE GPIO_MBED_ADAPTER::readPin(void)
{
    if (0 == this->my_pin.read())
    {
        return STATE::LOW;
    }
    else
    {
        return STATE::HIGH;
    }
}

}   // namespace IO
