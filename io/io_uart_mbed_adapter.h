
/*
 * file: io_uart_mbed_adapter.h
 * purpose: Inheritance of the UART interface for use with Mbed.
 */

#ifndef IO_UART_MBED_ADAPTER_H
#define IO_UART_MBED_ADAPTER_H

#include "io_uart_intf.h"
#include "mbed.h"

namespace IO
{

class uartMbedAdapter : public uartIntf
{
public:
    
    uartMbedAdapter();
    ~uartMbedAdapter();

    /*
     *  //////////////////////////////
     *  //WARNING: UNTESTED FUNCTION//
     *  //////////////////////////////
     */
    void baud(int baudrate) override;

    /*
     *  //////////////////////////////
     *  //WARNING: UNTESTED FUNCTION//
     *  //////////////////////////////
     */
    void format(int bits=8, Parity parityType=Parity::NONE, int stopBits=1) override;

    /*
     *  //////////////////////////////
     *  //WARNING: UNTESTED FUNCTION//
     *  //////////////////////////////
     */
    bool readable() override;

    /*
     *  //////////////////////////////
     *  //WARNING: UNTESTED FUNCTION//
     *  //////////////////////////////
     */
    bool writeable() override;
    
    /*
     *  //////////////////////////////
     *  //WARNING: UNTESTED FUNCTION//
     *  //////////////////////////////
     */
    void sendBreak() override;

    void putc(char c) override;
    void puts(const char *s) override;
    char getc() override;

    /*
     *  ////////////////////////////
     *  //WARNING: BROKEN FUNCTION//
     *  ////////////////////////////
     *  
     *  The function should return when it receives an EOF or newline, but this function
     *  only returns when the character limit is reached. This appears to be an issue with
     *  mbed's implementation since it is just a pass-through. (2/2/18)
     */
    char *gets(char *buf, int size) override;

    void printf(const char *format, ...) override;

private:

    mbed::Serial *my_mbedUART;

}; // class UART_MBED_ADAPTER
} // namespace IO
#endif // IO_UART_MBED_ADAPTER_H
