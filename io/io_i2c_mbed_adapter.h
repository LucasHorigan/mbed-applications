
/*
 * file: io_i2c_mbed_adapter.h
 * purpose: Inheritance of the I2C interface for use with Mbed.
 */

#ifndef IO_I2C_MBED_ADAPTER_H
#define IO_I2C_MBED_ADAPTER_H

#include "io_i2c_intf.h"
#include "mbed.h"

namespace IO
{

class i2cMbedAdapter : public i2cIntf
{
public:

    i2cMbedAdapter();
    ~i2cMbedAdapter();

    void write(uint8_t addr, char *data, int length) override;
    void read(uint8_t addr, char *data, int length) override;

private:
    I2C *my_mbedI2C;

}; // class I2C_MBED_ADAPTER
} // namespace IO
#endif // IO_I2C_MBED_ADAPTER_H
