import os

EVT_TARGETS = {
    'test_blink':
        {
            'sources': ['mbed-os', 'io', 'dev', os.path.join('testing', 'test_blink')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_i2c':
        {
            'sources': ['mbed-os', 'io', os.path.join('testing', 'test_i2c')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_uart':
        {
            'sources': ['mbed-os', 'io', 'dev', os.path.join('testing', 'test_uart')],
            'hw_target': 'NUCLEO_F302R8'
        },

}
