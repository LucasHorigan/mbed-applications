/*
 * file: main.cpp
 * purpose: Entry point of code execution for evt-bms.
 */

#include "mbed.h"
#include "task_manager.h"


int main()
{
    // initialization
    TASK_MANAGER myTaskManager;
    myTaskManager.init();

    while (true)
    {
        // loop forever
        myTaskManager.run();
        wait(0.001);
    }
}
