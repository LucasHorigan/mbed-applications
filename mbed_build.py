#!/usr/bin/python
from __future__ import print_function
import argparse
import evt_targets
import os
import subprocess


PROFILES = {
    'debug' : os.path.join('mbed-os', 'tools', 'profiles', 'debug.json'),
    'develop' : os.path.join('mbed-os', 'tools', 'profiles', 'develop.json'),
    'release' : os.path.join('mbed-os', 'tools', 'profiles', 'release.json')
}
BOARDS = {
    'nucleo' : 'NUCLEO_F302R8',
    'bms' : 'EVT_BMS_4_0',
    'imu' : 'EVT_IMU_3_0',
    'sgm' : 'EVT_SGM_1_0'
}
DEFAULT_TOOLCHAIN = 'GCC_ARM'


def main():
    """
    Builds a compile command from some arguments and a dictionary of targets.
    :return:
    """
    parser = argparse.ArgumentParser(description='Builds the long form command for compiling.')
    parser.add_argument('-t', '--target', required=True, help='The executable to be built.')
    parser.add_argument('-p', '--profile', help='The profile JSON to use for configuring compiler options.')
    parser.add_argument('-m', '--platform', help='The hardware platform to build for. Beware, code might not work on unsupported platforms.')
    parser.add_argument('-c', '--clean', action='store_true', help='Do a completely clean build.')
    my_arguments = parser.parse_args()

    print()

    if my_arguments.target not in evt_targets.EVT_TARGETS.keys():
        print("No target definition for " + my_arguments.target)
        my_target = None
        return
    else:
        my_target = my_arguments.target

    if my_arguments.platform is None:
        my_board = evt_targets.EVT_TARGETS[my_target]['hw_target']
    else:
        my_board = my_arguments.platform

    if my_arguments.profile is None or my_arguments.profile.lower() not in PROFILES.keys():
        my_profile = PROFILES['debug']
        if my_arguments.profile is not None:
            print(my_arguments.profile + " is not a profile option.")
        print("Using " + my_profile + '.')
    else:
        my_profile = PROFILES[my_arguments.profile.lower()]

    print()

    my_command = "mbed compile --build BUILD -m" + my_board + " --profile " + str(my_profile) + " -N " + my_target
    if my_arguments.clean:
        my_command += " -c"

    for source in evt_targets.EVT_TARGETS[my_target]['sources']:
        my_command += " --source " + str(source)

    try:
        print(subprocess.check_output(my_command.split()))
    except subprocess.CalledProcessError, e:
        print("Error During Compilation:\n" + e.output)


if __name__ == "__main__":
    main()
