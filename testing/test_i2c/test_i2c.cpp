/*
 * file: test_i2c.cpp
 * purpose: Tests i2c communications between the nucleo board and
 *          an i2c slave, using the mbed peripheral adapters.
 *          The test involves attempting to write several messages
 *          to an I2C slave at a specific address and then reading
 *          a message of "OK" from the slave. Because our platform
 *          is still in its infancy the printing and debugging must
 *          be done on the slave side.
 */

#include "mbed.h"
#include "io.h"


int main()
{
    constexpr uint8_t SLAVE_ADDR = 4;

    char msg_1[] = "I2C Test Message!";  // 17 chars
    char msg_2[] = "Requesting Data..."; // 19 chars
    char msg_3[] = "Data Received!";     // 14 chars
    char msg_3_alt[] = "Data Not Received!"; // 18 chars
    char buf[2];    // Space for the "OK" response

    IO::G_I2C_DRIVER.write(SLAVE_ADDR, msg_1, 17);
    IO::G_I2C_DRIVER.write(SLAVE_ADDR, msg_2, 19);
    IO::G_I2C_DRIVER.read(SLAVE_ADDR, buf, 2); // Try to read "OK"

    if (buf[0] == 'O' && buf[1] == 'K')
    {
        IO::G_I2C_DRIVER.write(SLAVE_ADDR, msg_3, 14);
    }
    else
    {
        IO::G_I2C_DRIVER.write(SLAVE_ADDR, msg_3_alt, 18);
    }

    for (;;) // Test over. Wait forever.
    {
        ; // null statement
    }
}
