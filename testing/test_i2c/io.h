/*
 * file: io.h
 * purpose: Holds access to the constructors for IO layer adapters.
 */

#ifndef IO_H
#define IO_H


#include "io_i2c_mbed_adapter.h"

namespace IO
{

// Peripheral adapters are statically allocated before main is called
i2cMbedAdapter G_I2C_DRIVER;

} // namespace IO


#endif // IO_H
