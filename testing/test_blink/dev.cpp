/*
 * file: dev.cpp
 * purpose: Holds the static initializers implementation for DEV objects.
 */

#include "dev.h"
#include "io.h"


namespace DEV
{

LED & my_blinking_led(void)
{
    // Allocated the first time and never deallocated
    static DEV::LED my_led(IO::LED_PIN);
    return my_led;
}

} //namespace DEV
