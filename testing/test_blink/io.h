/*
 * file: io.h
 * purpose: Holds access to the constructors for IO layer adapters.
 */

#ifndef IO_H
#define IO_H


#include "io_gpio_mbed_adapter.h"

namespace IO
{

// Peripheral adapters are statically allocated before main is called
GPIO_MBED_ADAPTER LED_PIN(GPIO_MBED_ADAPTER::PIN::MC_PB13);

} // namespace IO


#endif // IO_H
