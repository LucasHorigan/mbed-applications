/*
 * file: test_blink.cpp
 * purpose: Tests blinking an LED on the STM32 Nucleo development board using
 *          the Mbed peripheral adapters.
 */

#include "mbed.h"
#include "dev.h"


int main()
{
    DEV::LED & my_led = DEV::my_blinking_led();

    auto toggle_lambda = [& my_led]()
    {
        static bool state = false;
        state = !state;
        my_led.setCurrentState(static_cast<IO::GPIO_INTF::STATE>(state));
        return;
    };

    while (true)
    {
        toggle_lambda();
        wait(0.5);
    }
}
