# Test Plan for IO_UART Adapter

## Features
* Read and write characters and strings to a UART device.
* Perform formatted printed with printf.

## Required Hardware for Test
* STM32F302R8 Nucleo Development Board
* USB to TTL adapter

## Additional Required Software
* Terminal Emulator (i.e. Putty, minicom, screen)

## Steps to Complete the Test
1. Take the USB to TTL adapter and connect the Rx, Tx, and GND wires to pins PC_10, PC_11, and GND
    respectively on the Nucleo.
3. Build the executable with ```python mbed_build.py -t test_uart```
4. Drop the compiled ```test_uart.bin``` into the Nucleo's file system.
5. Open the terminal emulator and connect to the adapter. It should use a baud rate of 115200.
6. You should see a message that says "Welcome to UART!" followed by the messages "printf test!"
    and "printf test 2!"
7. After those messages there should be a prompt, ">", and any characters you type should be
    echoed back to you. If you press enter, you should be taken to a new line and given a new
    prompt.
