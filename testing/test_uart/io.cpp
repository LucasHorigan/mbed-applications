/*
 * file: io.cpp
 * purpose: Define the IO devices needed by the program.
 */

#include "io.h"

namespace IO
{
    namespace
    {
        uartMbedAdapter uart_driver;
        GPIO_MBED_ADAPTER led_pin(GPIO_MBED_ADAPTER::PIN::MC_PB13);
    }


    uartMbedAdapter &G_UART_DRIVER = uart_driver;
    GPIO_MBED_ADAPTER &G_LED_PIN = led_pin;

}
