/*
 * file: io.h
 * purpose: Holds access to the constructors for IO layer adapters.
 */

#ifndef IO_H
#define IO_H

#include "io_gpio_mbed_adapter.h"
#include "io_uart_mbed_adapter.h"

namespace IO
{

// Peripheral adapters are statically allocated before main is called
extern uartMbedAdapter &G_UART_DRIVER;
extern GPIO_MBED_ADAPTER &G_LED_PIN;

} // namespace IO


#endif // IO_H
