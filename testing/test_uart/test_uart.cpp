
#include "mbed.h"
#include "dev.h"
#include "io.h"


int main()
{
    constexpr unsigned BUFFER_SIZE = 100;
    char lineBuf[BUFFER_SIZE];
    char temp;

    DEV::LED & my_led = DEV::my_blinking_led();

    // Start by turning the LED off
    my_led.setCurrentState(IO::GPIO_INTF::STATE::HIGH);
    
    IO::G_UART_DRIVER.puts("Welcome to UART!\r\n");

    if (IO::G_UART_DRIVER.writeable())
    {
        // If it's writable turn the LED on
        my_led.setCurrentState(IO::GPIO_INTF::STATE::LOW);
    }

    // Make sure that printf works.
    IO::G_UART_DRIVER.printf("printf test!\r\n");
    IO::G_UART_DRIVER.printf("printf test %d!\r\n>", 2);

    for (;;) // Loop forever and echo the characters as the user types them.
    {
        temp = IO::G_UART_DRIVER.getc();
        IO::G_UART_DRIVER.putc(temp);

        if (temp == '\r') // If they hit enter, print a new line and new prompt.
        {
            IO::G_UART_DRIVER.puts("\n>");
        }
    }
}
