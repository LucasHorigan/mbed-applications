/*
 * file: dev.h
 * purpose: Holds the reference free functions for device objects.
 */

#ifndef DEV_H
#define DEV_H


#include "dev_led.h"

namespace DEV
{

// Order of static allocation between translation units is undefined so DEV and APP objects are accessed through a function
// which allocates them at first use.
LED & my_blinking_led(void);

} // namespace DEV

#endif // DEV_H
