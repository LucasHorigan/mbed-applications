/*
 * file: app.cpp
 * purpose: Entry point for synchronous calls to applications.
 */

#include "app.h"
#include "app_blink.h"

namespace APP
{

// Global constructors here
BLINK blink(5);


void init()
{
    // Call application initializations here.
    blink.init();
}


void one_second()
{
    // Called every one second
    blink.run();
}

} // namespace APP
