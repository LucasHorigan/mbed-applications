/*
 * file: app.h
 * purpose: A synchronous interface for running application drivers.
 *
 * Note that the functions declared here are free functions.
 */

#ifndef APP_H
#define APP_H

namespace APP
{

// Initializes applications.
void init();

// Call to be made to applications for tasks scheduled every one second.
void one_second();

}

#endif  // APP_H
