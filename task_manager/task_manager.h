/*
 * file: task_manager.h
 * purpose: Interface to the task manager application which drives functionality
 *			of the entire firmware.
 */

#ifndef TASK_MANAGER_H
#define TASK_MANAGER_H


#include <cstdint>


class TASK_MANAGER
{
public:

	// Constructor
	TASK_MANAGER();
	// Destructor
	~TASK_MANAGER();

	// Calls initialization code for all modules.
	void init();

	// Runs a cycle of the state machines for the applications and drivers.
	void run();

private:

	uint32_t my_tick_count = 0;
};

#endif	// TASK_MANAGER_H
