/*
 * file: task_manager.cpp
 * purpose: Implementation of the task manager for running modules synchronously.
 */

#include "app.h"
#include "task_manager.h"


TASK_MANAGER::TASK_MANAGER() : my_tick_count(0)
{
    // Constructor
    // Intentionally empty for now
}


TASK_MANAGER::~TASK_MANAGER()
{
    // Destructor
    // Intentionally empty for now
}


void TASK_MANAGER::init()
{
    APP::init();
    // DEV::init();
    // INTERRUPT_HANDLER::init();
    return;
}


void TASK_MANAGER::run()
{
    this->my_tick_count++;

    if (!(this->my_tick_count % 1))
    {
        // Runs every 1 millisecond
        // DEV::one_millisecond();
        // APP::one_millisecond();
    }

    if (!(this->my_tick_count % 10))
    {
        // Runs every 10 milliseconds
        // DEV::ten_millisecond();
        // APP::ten_millisecond();
    }

    if (!(this->my_tick_count % 100))
    {
        // Runs every 100 milliseconds
        // DEV::hundred_millisecond();
        // APP::hundred_millisecond();
    }

    if (1000 == this->my_tick_count)
    {
        // Runs every 1 second, resets the counter
        // DEV::one_second();
        APP::one_second();
        this->my_tick_count = 0;
    }
}
